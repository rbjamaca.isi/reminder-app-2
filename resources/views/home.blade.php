<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{csrf_token()}}" />

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />

        <title>Reminders App</title>

        <link rel="stylesheet" type="text/css" href="{{mix('/css/app.css')}}">
    </head>
    <body>
        <div id="app"></div>
        <script type="text/javascript" src="{{mix('/js/app.js')}}"></script>
    </body>
</html>