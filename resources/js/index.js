import React from 'react';
import ReactDOM from 'react-dom';
import {
    Route,
    BrowserRouter,
    Switch
} from 'react-router-dom'
import Home from './pages/Home';
import Login from './pages/Login'

const App = () => {
    return (
      <BrowserRouter>
        <div>
            <Switch>
              <Route exact path="/" component={Login} />
              <Route exact path="/home" component={Home} />
              <Route render={() => <h1>404: page not found</h1>} />
            </Switch>
        </div>
      </BrowserRouter>
    )
}

ReactDOM.render(
    <App />,
    document.getElementById('app')
)