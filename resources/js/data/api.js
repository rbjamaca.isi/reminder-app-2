import axios from 'axios'
import { getToken } from './auth';

export const API_POST = async (url, data) => {
    return await axios.post(url, data, {
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer '+ getToken()
        }
    }).then(res => {
        return res;
    })
}

export const API_GET = async (url) => {
    return await axios.get(url, {
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer '+ getToken()
        }
    }).then(res => {
        return res;
    })
}