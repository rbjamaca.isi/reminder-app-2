import { DialogContent } from '@material-ui/core'
import { Dialog } from '@material-ui/core'
import React, { useState, useEffect } from 'react'
import { API_GET } from '../data/api'


export default function Home() {
    const [data, setData] = useState([])
    const [open, setOpen] = useState(false)
    const getData = async () => {
        await API_GET('api/reminder').then(res => {
            // console.log(res.data)
            setData(res.data.data)
        })
    }

    useEffect(() => {
        getData();
    }, [])

    return (
        <div>
            <h1>My Reminders</h1>
            <button onClick={() => setOpen(true)} >add new reminder</button>
            <table>
                <thead>
                    <tr>
                        <td>Reminder</td>
                    </tr>
                </thead>
                <tbody>
                    {
                        data.map(reminder => 
                            <tr key={reminder.id}>
                                <td>{reminder.description}</td>
                            </tr>
                        )
                    }
                </tbody>
            </table>
            <Dialog open={open}>
                    <DialogContent>
                        
                    </DialogContent>
            </Dialog>
        </div>
    )
}