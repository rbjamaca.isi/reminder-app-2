<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $loginData = Validator::make($request->all(), [
            'email' => ['required', 'string'],
            'password' => ['required', 'string']
        ]);

        if ($loginData->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Validation error',
                'data' => $loginData->errors()
            ]);
        }

        if (!auth()->attempt($request->all())) {
            return response()->json([
                'success' => false,
                'message' => 'Invalid email or password provided.',
            ], 401);
        }

        $accessToken = auth()->user()->createToken('authToken')->accessToken;

        return response()->json([
            'success' => true,
            'user' => auth()->user(),
            'accessToken' => $accessToken
        ]);
    }

    public function register(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => ['required', 'string'],
            'email' => ['required', 'string'],
            'password' => ['required', 'string', 'confirmed']
        ]);

        if ($validation->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Validation error',
                'data' => $validation->errors()
            ]);
        }

        $request->merge(['password' => bcrypt($request->password)]);

        $user = User::create($request->all());

        $accessToken = $user->createToken('authToken')->accessToken;

        return response()->json([
            'success' => true,
            'user' => $user,
            'accessToken' => $accessToken
        ]);
    }

    public function logout()
    {
        if (auth()->user()) {
            $user = auth()->user()->token();
            $user->revoke();

            return response()->json([
                'success' => true,
                'message' => 'Successfully logged out.'
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Unauthenticated.'
        ]);
    }
}
